import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MaterialModule } from "../material/material.module";
import { BreadCrumbComponent } from "./bread-crumb.component";

@NgModule({
  declarations: [BreadCrumbComponent],
  imports: [CommonModule, MaterialModule, RouterModule],
  exports: [BreadCrumbComponent],
})
export class BreadCrumbModule {}
