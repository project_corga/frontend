import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { of } from "rxjs";
import { Section } from "../../core/models/section.model";
import { SectionService } from "../../core/services/sections.service";
import { BreadCrumbComponent } from "./bread-crumb.component";
import { BreadCrumbModule } from "./bread-crumb.module";

describe("BreadCrumbComponent", () => {
  let component: BreadCrumbComponent;
  let fixture: ComponentFixture<BreadCrumbComponent>;
  let sectionService: SectionService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          BreadCrumbModule,
          RouterTestingModule,
          HttpClientTestingModule,
        ],
        providers: [SectionService],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadCrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    sectionService = TestBed.inject(SectionService);
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should calculate breadCrumb on section-input", async () => {
    const input = mockSection("child", mockSection("root"));
    spyOn(sectionService, "getPath").and.returnValue(of([input]));
    component.section = input;

    const result = await component.breadCrumbs$.toPromise();
    expect(result).toEqual([
      { label: input.name, url: "/sections/" + input.id },
    ]);
  });
});

function mockSection(id: string, parent: Section = undefined): Section {
  if (parent != null) {
    parent.subSectionIds.push(id);
  }

  return {
    favorite: false,
    id,
    name: id,
    parentId: parent?.id,
    subSectionIds: [],
    cardIds: [],
  };
}
