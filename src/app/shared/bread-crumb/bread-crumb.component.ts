import { Component, Input } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Section } from "../../core/models/section.model";
import { SectionService } from "../../core/services/sections.service";
import { BreadCrumb } from "./bread-crumb.model";

@Component({
  selector: "app-bread-crumb",
  templateUrl: "./bread-crumb.component.html",
  styleUrls: ["./bread-crumb.component.scss"],
})
export class BreadCrumbComponent {
  breadCrumbs$: Observable<BreadCrumb[]>;

  @Input()
  set section(section: Section) {
    if (section == null) return;
    this.breadCrumbs$ = this.calculateBreadCrumb(section);
  }

  constructor(private sectionService: SectionService) {}

  private calculateBreadCrumb(section: Section): Observable<BreadCrumb[]> {
    return this.sectionService.getPath(section.id).pipe(
      map((sections) => sections.reverse()),
      map((sections) =>
        sections.map((s) => ({
          label: s.name,
          url: "/sections/" + s.id,
        }))
      )
    );
  }
}
