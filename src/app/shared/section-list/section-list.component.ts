import { Component, Input } from "@angular/core";
import { SectionService } from "src/app/core/services/sections.service";
import { Section } from "../../core/models/section.model";

@Component({
  selector: "app-section-list",
  templateUrl: "./section-list.component.html",
  styleUrls: ["./section-list.component.scss"],
})
export class SectionListComponent {
  constructor(private sectionService: SectionService) {}

  @Input() header: string;
  @Input() sections: Section[];

  delete(section: Section): void {
    this.sectionService.delete(section.id);
  }

  toggleFavorite(section: Section): void {
    this.sectionService.setFavorite(section.id, !section.favorite);
  }
}
