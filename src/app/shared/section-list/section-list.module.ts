import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { DeleteSectionButtonModule } from "../buttons/delete-section-button/delete-section-button.module";
import { UpdateSectionButtonModule } from "../buttons/update-section-button/update-section-button.module";
import { MaterialModule } from "../material/material.module";
import { SectionListComponent } from "./section-list.component";

@NgModule({
  declarations: [SectionListComponent],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    DeleteSectionButtonModule,
    UpdateSectionButtonModule,
  ],
  exports: [SectionListComponent],
})
export class SectionListModule {}
