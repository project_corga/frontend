import { NgModule } from "@angular/core";
import { BreadCrumbModule } from "./bread-crumb/bread-crumb.module";
import { CreateCardButtonModule } from "./buttons/create-card-button/create-card-button.module";
import { CreateSectionButtonModule } from "./buttons/create-section-button/create-section-button.module";
import { DeleteCardButtonModule } from "./buttons/delete-card-button/delete-card-button.module";
import { DeleteSectionButtonModule } from "./buttons/delete-section-button/delete-section-button.module";
import { UpdateCardButtonModule } from "./buttons/update-card-button/update-card-button.module";
import { UpdateSectionButtonModule } from "./buttons/update-section-button/update-section-button.module";
import { CardListModule } from "./card-list/card-list.module";
import { FooterModule } from "./footer/footer.module";
import { HeaderModule } from "./header/header.module";
import { MaterialModule } from "./material/material.module";
import { SectionListModule } from "./section-list/section-list.module";

@NgModule({
  declarations: [],
  imports: [
    BreadCrumbModule,
    CreateSectionButtonModule,
    DeleteSectionButtonModule,
    FooterModule,
    HeaderModule,
    SectionListModule,
    UpdateSectionButtonModule,
    CreateCardButtonModule,
    DeleteCardButtonModule,
    CardListModule,
    UpdateCardButtonModule,
    MaterialModule,
  ],
  exports: [
    BreadCrumbModule,
    CreateSectionButtonModule,
    DeleteSectionButtonModule,
    FooterModule,
    HeaderModule,
    SectionListModule,
    UpdateSectionButtonModule,
    CreateCardButtonModule,
    DeleteCardButtonModule,
    CardListModule,
    UpdateCardButtonModule,
    MaterialModule,
  ],
})
export class SharedModule {}
