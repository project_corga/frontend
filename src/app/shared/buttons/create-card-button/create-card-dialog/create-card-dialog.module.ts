import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "../../../material/material.module";
import { CreateCardDialogComponent } from "./create-card-dialog.component";

@NgModule({
  declarations: [CreateCardDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserAnimationsModule,
  ],
  exports: [CreateCardDialogComponent],
})
export class CreateCardDialogModule {}
