import { Component, Inject } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { CreateCard } from "../../../../core/models/card.model";
import { Section } from "../../../../core/models/section.model";
import { CardService } from "../../../../core/services/cards.service";

@Component({
  selector: "app-create-card-dialog",
  templateUrl: "./create-card-dialog.component.html",
  styleUrls: ["./create-card-dialog.component.scss"],
})
export class CreateCardDialogComponent {
  constructor(
    private formBuilder: FormBuilder,
    private cardService: CardService,
    private dialogRef: MatDialogRef<CreateCardDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private section: Section
  ) {}

  form: FormGroup;

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: ["", [Validators.required]],
      content: "",
      favorite: false,
    });
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    const data: CreateCard = {
      ...this.form.value,
      sectionId: this.section.id,
    };
    this.cardService.create(data);
    this.dialogRef.close();
  }
}
