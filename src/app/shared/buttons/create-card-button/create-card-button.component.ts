import { Component, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Section } from "../../../core/models/section.model";
import { CreateCardDialogComponent } from "./create-card-dialog/create-card-dialog.component";

@Component({
  selector: "app-create-card-button",
  templateUrl: "./create-card-button.component.html",
  styleUrls: ["./create-card-button.component.scss"],
})
export class CreateCardButtonComponent {
  constructor(private dialog: MatDialog) {}

  @Input() section: Section;

  openDialog(): void {
    this.dialog.open(CreateCardDialogComponent, {
      data: this.section,
    });
  }
}
