import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MaterialModule } from "../../material/material.module";
import { CreateCardButtonComponent } from "./create-card-button.component";
import { CreateCardDialogModule } from "./create-card-dialog/create-card-dialog.module";

@NgModule({
  declarations: [CreateCardButtonComponent],
  imports: [CommonModule, MaterialModule, CreateCardDialogModule],
  exports: [CreateCardButtonComponent],
})
export class CreateCardButtonModule {}
