import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { MatDialog } from "@angular/material/dialog";
import { CreateSectionButtonComponent } from "./create-section-button.component";
import { CreateSectionButtonModule } from "./create-section-button.module";

describe("CreateSectionButtonComponent", () => {
  let component: CreateSectionButtonComponent;
  let fixture: ComponentFixture<CreateSectionButtonComponent>;
  let dialog: MatDialog;
  let button: HTMLButtonElement;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [CreateSectionButtonModule],
      }).compileComponents();
      dialog = TestBed.inject(MatDialog);
      spyOn(dialog, "open");
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSectionButtonComponent);
    component = fixture.componentInstance;
    button = fixture.nativeElement.querySelector("button");
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should have button to add a new section", () => {
    expect(button.disabled).toBe(false);
  });

  it("should open dialog if button is clicked", () => {
    expect(button.disabled).toBe(false);
    button.click();
    expect(dialog.open).toHaveBeenCalledTimes(1);
  });
});
