import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MaterialModule } from "../../material/material.module";
import { CreateSectionButtonComponent } from "./create-section-button.component";
import { CreateSectionDialogModule } from "./create-section-dialog/create-section-dialog.module";

@NgModule({
  declarations: [CreateSectionButtonComponent],
  imports: [CommonModule, MaterialModule, CreateSectionDialogModule],
  exports: [CreateSectionButtonComponent],
})
export class CreateSectionButtonModule {}
