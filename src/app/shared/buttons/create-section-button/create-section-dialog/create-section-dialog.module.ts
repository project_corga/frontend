import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "../../../material/material.module";
import { CreateSectionDialogComponent } from "./create-section-dialog.component";

@NgModule({
  declarations: [CreateSectionDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserAnimationsModule,
  ],
  exports: [CreateSectionDialogComponent],
})
export class CreateSectionDialogModule {}
