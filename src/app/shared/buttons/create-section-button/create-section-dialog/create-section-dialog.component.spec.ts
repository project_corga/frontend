import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { FormBuilder } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SectionService } from "../../../../core/services/sections.service";
import { mockSection } from "../../../../test.util";
import { CreateSectionDialogComponent } from "./create-section-dialog.component";
import { CreateSectionDialogModule } from "./create-section-dialog.module";

describe("CreateSectionDialogComponent", () => {
  let component: CreateSectionDialogComponent;
  let fixture: ComponentFixture<CreateSectionDialogComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [CreateSectionDialogModule, HttpClientTestingModule],
        providers: [
          FormBuilder,
          SectionService,
          { provide: MatDialogRef, useValue: {} },
          { provide: MAT_DIALOG_DATA, useValue: mockSection() },
        ],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSectionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
