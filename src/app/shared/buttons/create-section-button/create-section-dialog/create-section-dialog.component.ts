import { Component, Inject } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { CreateSection, Section } from "../../../../core/models/section.model";
import { SectionService } from "../../../../core/services/sections.service";

@Component({
  selector: "app-create-section-dialog",
  templateUrl: "./create-section-dialog.component.html",
  styleUrls: ["./create-section-dialog.component.scss"],
})
export class CreateSectionDialogComponent {
  constructor(
    private formBuilder: FormBuilder,
    private sectionService: SectionService,
    private dialogRef: MatDialogRef<CreateSectionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private parent: Section
  ) {}

  form: FormGroup;

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: ["", [Validators.required]],
      favorite: false,
    });
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    const data: CreateSection = {
      ...this.form.value,
      parentId: this.parent.id,
    };
    this.sectionService.create(data);
    this.dialogRef.close();
  }
}
