import { Component, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Section } from "../../../core/models/section.model";
import { CreateSectionDialogComponent } from "./create-section-dialog/create-section-dialog.component";

@Component({
  selector: "app-create-section-button",
  templateUrl: "./create-section-button.component.html",
  styleUrls: ["./create-section-button.component.scss"],
})
export class CreateSectionButtonComponent {
  constructor(private dialog: MatDialog) {}

  @Input() parent: Section;

  openDialog(): void {
    this.dialog.open(CreateSectionDialogComponent, {
      data: this.parent,
    });
  }
}
