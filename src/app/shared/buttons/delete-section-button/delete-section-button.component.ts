import { Component, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Section } from "../../../core/models/section.model";
import { DeleteSectionDialogComponent } from "./delete-section-dialog/delete-section-dialog.component";

@Component({
  selector: "app-delete-section-button",
  templateUrl: "./delete-section-button.component.html",
  styleUrls: ["./delete-section-button.component.scss"],
})
export class DeleteSectionButtonComponent {
  constructor(private dialog: MatDialog) {}

  @Input() section: Section;

  openDialog(): void {
    this.dialog.open(DeleteSectionDialogComponent, {
      data: this.section,
    });
  }
}
