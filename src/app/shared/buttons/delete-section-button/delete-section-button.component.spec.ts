import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { MatDialog } from "@angular/material/dialog";
import { DeleteSectionButtonComponent } from "./delete-section-button.component";
import { DeleteSectionButtonModule } from "./delete-section-button.module";

describe("DeleteSectionButtonComponent", () => {
  let component: DeleteSectionButtonComponent;
  let fixture: ComponentFixture<DeleteSectionButtonComponent>;
  let dialog: MatDialog;
  let button: HTMLButtonElement;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [DeleteSectionButtonModule],
      }).compileComponents();
      dialog = TestBed.inject(MatDialog);
      spyOn(dialog, "open");
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteSectionButtonComponent);
    component = fixture.componentInstance;
    button = fixture.nativeElement.querySelector("button");
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should have button to delete a section", () => {
    expect(button.disabled).toBe(false);
  });

  it("should open dialog if button is clicked", () => {
    expect(button.disabled).toBe(false);
    button.click();
    expect(dialog.open).toHaveBeenCalledTimes(1);
  });
});
