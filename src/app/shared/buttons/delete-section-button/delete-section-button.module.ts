import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MaterialModule } from "../../material/material.module";
import { DeleteSectionButtonComponent } from "./delete-section-button.component";
import { DeleteSectionDialogModule } from "./delete-section-dialog/delete-section-dialog.module";

@NgModule({
  declarations: [DeleteSectionButtonComponent],
  imports: [CommonModule, MaterialModule, DeleteSectionDialogModule],
  exports: [DeleteSectionButtonComponent],
})
export class DeleteSectionButtonModule {}
