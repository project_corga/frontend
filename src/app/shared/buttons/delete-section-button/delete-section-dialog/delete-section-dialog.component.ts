import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Section } from "../../../../core/models/section.model";
import { SectionService } from "../../../../core/services/sections.service";
import { CreateSectionDialogComponent } from "../../create-section-button/create-section-dialog/create-section-dialog.component";

@Component({
  selector: "app-delete-section-dialog",
  templateUrl: "./delete-section-dialog.component.html",
  styleUrls: ["./delete-section-dialog.component.scss"],
})
export class DeleteSectionDialogComponent {
  constructor(
    private sectionService: SectionService,
    private dialogRef: MatDialogRef<CreateSectionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public section: Section
  ) {}

  onCancel() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.sectionService.delete(this.section.id);
    this.dialogRef.close();
  }
}
