import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "../../../material/material.module";
import { DeleteSectionDialogComponent } from "./delete-section-dialog.component";

@NgModule({
  declarations: [DeleteSectionDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserAnimationsModule,
  ],
  exports: [DeleteSectionDialogComponent],
})
export class DeleteSectionDialogModule {}
