import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { mockSection } from "../../../../test.util";
import { DeleteSectionDialogComponent } from "./delete-section-dialog.component";
import { DeleteSectionDialogModule } from "./delete-section-dialog.module";

describe("DeleteSectionDialogComponent", () => {
  let component: DeleteSectionDialogComponent;
  let fixture: ComponentFixture<DeleteSectionDialogComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [DeleteSectionDialogModule, HttpClientTestingModule],
        providers: [
          { provide: MatDialogRef, useValue: {} },
          { provide: MAT_DIALOG_DATA, useValue: mockSection() },
        ],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteSectionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
