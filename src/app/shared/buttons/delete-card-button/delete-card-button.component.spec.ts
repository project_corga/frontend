import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { MatDialog } from "@angular/material/dialog";
import { DeleteCardButtonComponent } from "./delete-card-button.component";
import { DeleteCardButtonModule } from "./delete-card-button.module";

describe("DeleteCardButtonComponent", () => {
  let component: DeleteCardButtonComponent;
  let fixture: ComponentFixture<DeleteCardButtonComponent>;
  let dialog: MatDialog;
  let button: HTMLButtonElement;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [DeleteCardButtonModule],
      }).compileComponents();
      dialog = TestBed.inject(MatDialog);
      spyOn(dialog, "open");
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteCardButtonComponent);
    component = fixture.componentInstance;
    button = fixture.nativeElement.querySelector("button");
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should have button to delete a card", () => {
    expect(button.disabled).toBe(false);
  });

  it("should open dialog if button is clicked", () => {
    expect(button.disabled).toBe(false);
    button.click();
    expect(dialog.open).toHaveBeenCalledTimes(1);
  });
});
