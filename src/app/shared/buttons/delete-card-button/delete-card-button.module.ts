import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MaterialModule } from "../../material/material.module";
import { DeleteCardButtonComponent } from "./delete-card-button.component";
import { DeleteCardDialogModule } from "./delete-card-dialog/delete-card-dialog.module";

@NgModule({
  declarations: [DeleteCardButtonComponent],
  imports: [CommonModule, MaterialModule, DeleteCardDialogModule],
  exports: [DeleteCardButtonComponent],
})
export class DeleteCardButtonModule {}
