import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Card } from "../../../../core/models/card.model";
import { CardService } from "../../../../core/services/cards.service";
import { CreateCardDialogComponent } from "../../create-card-button/create-card-dialog/create-card-dialog.component";

@Component({
  selector: "app-delete-card-dialog",
  templateUrl: "./delete-card-dialog.component.html",
  styleUrls: ["./delete-card-dialog.component.scss"],
})
export class DeleteCardDialogComponent {
  constructor(
    private cardService: CardService,
    private dialogRef: MatDialogRef<CreateCardDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public card: Card
  ) {}

  onCancel() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.cardService.delete(this.card.id);
    this.dialogRef.close();
  }
}
