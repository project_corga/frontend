import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "../../../material/material.module";
import { DeleteCardDialogComponent } from "./delete-card-dialog.component";

@NgModule({
  declarations: [DeleteCardDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserAnimationsModule,
  ],
  exports: [DeleteCardDialogComponent],
})
export class DeleteCardDialogModule {}
