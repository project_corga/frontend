import { Component, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Card } from "../../../core/models/card.model";
import { DeleteCardDialogComponent } from "./delete-card-dialog/delete-card-dialog.component";

@Component({
  selector: "app-delete-card-button",
  templateUrl: "./delete-card-button.component.html",
  styleUrls: ["./delete-card-button.component.scss"],
})
export class DeleteCardButtonComponent {
  constructor(private dialog: MatDialog) {}

  @Input() card: Card;

  openDialog(): void {
    this.dialog.open(DeleteCardDialogComponent, {
      data: this.card,
    });
  }
}
