import { Component, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Section } from "src/app/core/models/section.model";
import { UpdateSectionDialogComponent } from "./update-section-dialog/update-section-dialog.component";

@Component({
  selector: "app-update-section-button",
  templateUrl: "./update-section-button.component.html",
  styleUrls: ["./update-section-button.component.scss"],
})
export class UpdateSectionButtonComponent {
  constructor(private dialog: MatDialog) {}

  @Input() section: Section;

  openDialog(): void {
    this.dialog.open(UpdateSectionDialogComponent, {
      data: this.section,
    });
  }
}
