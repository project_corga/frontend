import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { MatDialog } from "@angular/material/dialog";
import { UpdateSectionButtonComponent } from "./update-section-button.component";
import { UpdateSectionButtonModule } from "./update-section-button.module";

describe("UpdateSectionButtonComponent", () => {
  let component: UpdateSectionButtonComponent;
  let fixture: ComponentFixture<UpdateSectionButtonComponent>;
  let dialog: MatDialog;
  let button: HTMLButtonElement;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [UpdateSectionButtonModule],
        providers: [MatDialog],
      }).compileComponents();
      dialog = TestBed.inject(MatDialog);
      spyOn(dialog, "open");
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSectionButtonComponent);
    component = fixture.componentInstance;
    button = fixture.nativeElement.querySelector("button");
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should have button to update a section", () => {
    expect(button.disabled).toBe(false);
  });

  it("should open dialog if button is clicked", () => {
    expect(button.disabled).toBe(false);
    button.click();
    expect(dialog.open).toHaveBeenCalledTimes(1);
  });
});
