import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MaterialModule } from "../../material/material.module";
import { UpdateSectionButtonComponent } from "./update-section-button.component";
import { UpdateSectionDialogModule } from "./update-section-dialog/update-section-dialog.module";

@NgModule({
  declarations: [UpdateSectionButtonComponent],
  imports: [CommonModule, MaterialModule, UpdateSectionDialogModule],
  exports: [UpdateSectionButtonComponent],
})
export class UpdateSectionButtonModule {}
