import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { FormBuilder } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SectionService } from "../../../../core/services/sections.service";
import { mockSection } from "../../../../test.util";
import { UpdateSectionDialogComponent } from "./update-section-dialog.component";
import { UpdateSectionDialogModule } from "./update-section-dialog.module";

describe("UpdateSectionDialogComponent", () => {
  let component: UpdateSectionDialogComponent;
  let fixture: ComponentFixture<UpdateSectionDialogComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [UpdateSectionDialogModule, HttpClientTestingModule],
        providers: [
          FormBuilder,
          SectionService,
          { provide: MatDialogRef, useValue: {} },
          { provide: MAT_DIALOG_DATA, useValue: mockSection() },
        ],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSectionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
