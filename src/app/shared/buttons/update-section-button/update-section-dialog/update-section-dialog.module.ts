import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "../../../material/material.module";
import { UpdateSectionDialogComponent } from "./update-section-dialog.component";

@NgModule({
  declarations: [UpdateSectionDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserAnimationsModule,
  ],
  exports: [UpdateSectionDialogComponent],
})
export class UpdateSectionDialogModule {}
