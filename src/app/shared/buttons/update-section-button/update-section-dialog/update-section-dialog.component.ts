import { Component, Inject, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Section, UpdateSection } from "src/app/core/models/section.model";
import { SectionService } from "../../../../core/services/sections.service";

@Component({
  selector: "app-update-section-dialog",
  templateUrl: "./update-section-dialog.component.html",
  styleUrls: ["./update-section-dialog.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class UpdateSectionDialogComponent implements OnInit {
  constructor(
    private sectionService: SectionService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<
      UpdateSectionDialogComponent,
      UpdateSection
    >,
    @Inject(MAT_DIALOG_DATA) private data: Section
  ) {}

  form: FormGroup;

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: [this.data.name, [Validators.required]],
      favorite: this.data.favorite,
    });
  }

  onSubmit() {
    const data: UpdateSection = this.form.value;
    this.sectionService.update(this.data.id, data);
    this.dialogRef.close();
  }

  onCancel() {
    this.dialogRef.close();
  }
}
