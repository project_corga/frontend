import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MaterialModule } from "../../material/material.module";
import { UpdateCardButtonComponent } from "./update-card-button.component";
import { UpdateCardDialogModule } from "./update-card-dialog/update-card-dialog.module";

@NgModule({
  declarations: [UpdateCardButtonComponent],
  imports: [CommonModule, MaterialModule, UpdateCardDialogModule],
  exports: [UpdateCardButtonComponent],
})
export class UpdateCardButtonModule {}
