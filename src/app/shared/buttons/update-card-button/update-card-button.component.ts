import { Component, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Card } from "../../../core/models/card.model";
import { UpdateCardDialogComponent } from "./update-card-dialog/update-card-dialog.component";

@Component({
  selector: "app-update-card-button",
  templateUrl: "./update-card-button.component.html",
  styleUrls: ["./update-card-button.component.scss"],
})
export class UpdateCardButtonComponent {
  constructor(private dialog: MatDialog) {}

  @Input() card: Card;

  openDialog(): void {
    this.dialog.open(UpdateCardDialogComponent, {
      data: this.card,
    });
  }
}
