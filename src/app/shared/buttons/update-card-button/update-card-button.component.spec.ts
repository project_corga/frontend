import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { MatDialog } from "@angular/material/dialog";
import { UpdateCardButtonComponent } from "./update-card-button.component";
import { UpdateCardButtonModule } from "./update-card-button.module";

describe("UpdateCardButtonComponent", () => {
  let component: UpdateCardButtonComponent;
  let fixture: ComponentFixture<UpdateCardButtonComponent>;
  let dialog: MatDialog;
  let button: HTMLButtonElement;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [UpdateCardButtonModule],
        providers: [MatDialog],
      }).compileComponents();
      dialog = TestBed.inject(MatDialog);
      spyOn(dialog, "open");
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCardButtonComponent);
    component = fixture.componentInstance;
    button = fixture.nativeElement.querySelector("button");
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should have button to update a card", () => {
    expect(button.disabled).toBe(false);
  });

  it("should open dialog if button is clicked", () => {
    expect(button.disabled).toBe(false);
    button.click();
    expect(dialog.open).toHaveBeenCalledTimes(1);
  });
});
