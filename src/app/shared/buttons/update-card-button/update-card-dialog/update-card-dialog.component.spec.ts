import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { FormBuilder } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { CardService } from "../../../../core/services/cards.service";
import { mockCard } from "../../../../test.util";
import { UpdateCardDialogComponent } from "./update-card-dialog.component";
import { UpdateCardDialogModule } from "./update-card-dialog.module";

describe("UpdateCardDialogComponent", () => {
  let component: UpdateCardDialogComponent;
  let fixture: ComponentFixture<UpdateCardDialogComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [UpdateCardDialogModule, HttpClientTestingModule],
        providers: [
          FormBuilder,
          CardService,
          { provide: MatDialogRef, useValue: {} },
          { provide: MAT_DIALOG_DATA, useValue: mockCard() },
        ],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCardDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
