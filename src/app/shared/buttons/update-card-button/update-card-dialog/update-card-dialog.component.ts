import { Component, Inject, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Card, UpdateCard } from "src/app/core/models/Card.model";
import { CardService } from "../../../../core/services/cards.service";

@Component({
  selector: "app-update-card-dialog",
  templateUrl: "./update-card-dialog.component.html",
  styleUrls: ["./update-card-dialog.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class UpdateCardDialogComponent implements OnInit {
  constructor(
    private cardService: CardService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<UpdateCardDialogComponent, UpdateCard>,
    @Inject(MAT_DIALOG_DATA) private data: Card
  ) {}

  form: FormGroup;

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: [this.data.name, [Validators.required]],
      content: this.data.content,
      favorite: this.data.favorite,
    });
  }

  onSubmit() {
    const data: UpdateCard = this.form.value;
    this.cardService.update(this.data.id, data);
    this.dialogRef.close();
  }

  onCancel() {
    this.dialogRef.close();
  }
}
