import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "../../../material/material.module";
import { UpdateCardDialogComponent } from "./update-card-dialog.component";

@NgModule({
  declarations: [UpdateCardDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserAnimationsModule,
  ],
  exports: [UpdateCardDialogComponent],
})
export class UpdateCardDialogModule {}
