import { Component, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Card } from "../../core/models/card.model";
import { CardService } from "../../core/services/cards.service";
import { CardsComponent } from "../../views/card/cards.component";

@Component({
  selector: "app-card-list",
  templateUrl: "./card-list.component.html",
  styleUrls: ["./card-list.component.scss"],
})
export class CardListComponent {
  constructor(
    private cardService: CardService,
    private cardDialog: MatDialog
  ) {}

  @Input() header: string;
  @Input() cards: Card[];

  delete(card: Card): void {
    this.cardService.delete(card.id);
  }

  toggleFavorite(card: Card): void {
    this.cardService.setFavorite(card.id, !card.favorite);
  }

  showCard(card: Card): void {
    this.cardDialog.open(CardsComponent, {
      data: card,
    });
  }
}
