import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { DeleteCardButtonModule } from "../buttons/delete-card-button/delete-card-button.module";
import { UpdateCardButtonModule } from "../buttons/update-card-button/update-card-button.module";
import { MaterialModule } from "../material/material.module";
import { CardListComponent } from "./card-list.component";

@NgModule({
  declarations: [CardListComponent],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    DeleteCardButtonModule,
    UpdateCardButtonModule,
  ],
  exports: [CardListComponent],
})
export class CardListModule {}
