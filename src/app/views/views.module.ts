import { NgModule } from "@angular/core";
import { CardsModule } from "./card/cards.module";
import { DashboardModule } from "./dashboard/dashboard.module";
import { ImpressumModule } from "./impressum/impressum.module";
import { SectionsModule } from "./sections/sections.module";

@NgModule({
  declarations: [],
  imports: [DashboardModule, SectionsModule, ImpressumModule, CardsModule],
  exports: [DashboardModule, SectionsModule, ImpressumModule, CardsModule],
})
export class ViewsModule {}
