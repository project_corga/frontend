import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Observable } from "rxjs";
import { Card } from "../../core/models/card.model";
import { Section } from "../../core/models/section.model";
import { CardService } from "../../core/services/cards.service";
import { SectionService } from "../../core/services/sections.service";

export interface DialogData {
  sectionName: string;
}

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
})
export class DashboardComponent implements OnInit {
  sections$: Observable<Section[]>;
  favSections$: Observable<Section[]>;
  favCards$: Observable<Card[]>;
  rootSection$: Observable<Section>;

  constructor(
    public dialog: MatDialog,
    private sectionService: SectionService,
    private cardService: CardService
  ) {}

  ngOnInit() {
    this.sections$ = this.sectionService.getTopLevel();
    this.favSections$ = this.sectionService.getFavorites();
    this.favCards$ = this.cardService.getFavorites();
    this.rootSection$ = this.sectionService.getRoot();
  }
}
