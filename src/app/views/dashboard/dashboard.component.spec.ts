import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { SectionService } from "../../core/services/sections.service";
import { HeaderModule } from "../../shared/header/header.module";
import { DashboardComponent } from "./dashboard.component";
import { DashboardModule } from "./dashboard.module";

describe("DashboardComponent", () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let domReference;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          DashboardModule,
          RouterTestingModule,
          HttpClientTestingModule,
          HeaderModule,
        ],
        providers: [SectionService],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    domReference = fixture.debugElement.nativeElement;
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
  it("should contain header", () => {
    expect(domReference.querySelector("app-header")).toBeTruthy();
  });
});
