import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "../../shared/shared.module";
import { ImpressumComponent } from "./impressum.component";

@NgModule({
  declarations: [ImpressumComponent],
  imports: [CommonModule, SharedModule],
  exports: [ImpressumComponent],
})
export class ImpressumModule {}
