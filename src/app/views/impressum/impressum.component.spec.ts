import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { HeaderModule } from "../../shared/header/header.module";
import { ImpressumComponent } from "./impressum.component";
import { ImpressumModule } from "./impressum.module";

describe("ImpressumComponent", () => {
  let component: ImpressumComponent;
  let fixture: ComponentFixture<ImpressumComponent>;
  let domReference;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [ImpressumModule, RouterTestingModule, HeaderModule],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ImpressumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    domReference = fixture.debugElement.nativeElement;
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
  it("should contain header", () => {
    expect(domReference.querySelector("app-header")).toBeTruthy();
  });
});
