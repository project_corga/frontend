import { DragDropModule } from "@angular/cdk/drag-drop";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "../../shared/material/material.module";
import { SharedModule } from "../../shared/shared.module";
import { CardsComponent } from "./cards.component";

@NgModule({
  declarations: [CardsComponent],
  imports: [
    CommonModule,
    DragDropModule,
    SharedModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule,
  ],
  exports: [CardsComponent],
})
export class CardsModule {}
