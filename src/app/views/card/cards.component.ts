import { Component, Inject, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { Card, UpdateCard } from "../../core/models/card.model";
import { CardService } from "../../core/services/cards.service";

@Component({
  selector: "app-cards",
  templateUrl: "./cards.component.html",
  styleUrls: ["./cards.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class CardsComponent implements OnInit {
  constructor(
    private cardService: CardService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private dialogRef: MatDialogRef<CardsComponent, UpdateCard>,
    @Inject(MAT_DIALOG_DATA) private cardData: Card
  ) {
    dialogRef.disableClose = true;
  }

  form: FormGroup;
  card: Card;
  current$: Observable<Card>;

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: [this.cardData.name, [Validators.required]],
      content: this.cardData.content,
      favorite: this.cardData.favorite,
    });
    this.current$ = this.cardService.getByID(this.cardData.id);
    this.current$.subscribe((car) => (this.card = car));
  }
  close() {
    this.dialogRef.close();
  }
}
