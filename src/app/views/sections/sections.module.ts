import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "../../shared/shared.module";
import { SectionsComponent } from "./sections.component";

@NgModule({
  declarations: [SectionsComponent],
  imports: [CommonModule, SharedModule],
  exports: [SectionsComponent],
})
export class SectionsModule {}
