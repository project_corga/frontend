import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { of } from "rxjs";
import { CardService } from "../../core/services/cards.service";
import { SectionService } from "../../core/services/sections.service";
import { HeaderModule } from "../../shared/header/header.module";
import { mockSection } from "../../test.util";
import { SectionsComponent } from "./sections.component";
import { SectionsModule } from "./sections.module";

describe("SectionsComponent", () => {
  let component: SectionsComponent;
  let fixture: ComponentFixture<SectionsComponent>;
  let sectionService: SectionService;
  let cardService: CardService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          RouterTestingModule,
          SectionsModule,
          HttpClientTestingModule,
          HeaderModule,
        ],
      }).compileComponents();
      sectionService = TestBed.inject(SectionService);
      cardService = TestBed.inject(CardService);
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    const section = mockSection();
    const subSections = [mockSection(), mockSection()];
    spyOn(sectionService, "getByID").and.returnValue(of(section));
    spyOn(sectionService, "getSubSections").and.returnValue(of(subSections));
    spyOn(cardService, "getBySectionID").and.returnValue(of([]));

    expect(component).toBeTruthy();
  });
  it("should contain header", () => {
    expect(fixture.nativeElement.querySelector("app-header")).toBeTruthy();
  });
});
