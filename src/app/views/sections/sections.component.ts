import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { switchMap } from "rxjs/operators";
import { Card } from "../../core/models/card.model";
import { Section } from "../../core/models/section.model";
import { CardService } from "../../core/services/cards.service";
import { SectionService } from "../../core/services/sections.service";

@Component({
  selector: "app-sections",
  templateUrl: "./sections.component.html",
  styleUrls: ["./sections.component.scss"],
})
export class SectionsComponent implements OnInit {
  constructor(
    private sectionService: SectionService,
    private cardService: CardService,
    private route: ActivatedRoute
  ) {}

  current$: Observable<Section>;
  currentSubSections$: Observable<Section[]>;
  currentCards$: Observable<Card[]>;

  ngOnInit() {
    this.current$ = this.route.params.pipe(
      switchMap((params) => this.sectionService.getByID(params["id"]))
    );
    this.currentSubSections$ = this.route.params.pipe(
      switchMap((params) => this.sectionService.getSubSections(params["id"]))
    );
    this.currentCards$ = this.route.params.pipe(
      switchMap((params) => this.cardService.getBySectionID(params["id"]))
    );
  }

  toggleFavorite(section: Section): void {
    this.sectionService.setFavorite(section.id, !section.favorite);
  }
}
