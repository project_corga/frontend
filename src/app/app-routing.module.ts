import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DashboardComponent } from "./views/dashboard/dashboard.component";
import { ImpressumComponent } from "./views/impressum/impressum.component";
import { SectionsComponent } from "./views/sections/sections.component";

const routes: Routes = [
  { path: "sections/:id", component: SectionsComponent },
  { path: "dashboard", component: DashboardComponent },
  { path: "impressum", component: ImpressumComponent },
  { path: "", redirectTo: "dashboard", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: "legacy" })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
