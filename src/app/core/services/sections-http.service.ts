import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { shareReplay } from "rxjs/operators";
import { CreateSection, Section, UpdateSection } from "../models/section.model";
import { CardHttpService } from "./cards-http.service";

@Injectable({
  providedIn: "root",
})
export class SectionHttpService {
  private _sections$: BehaviorSubject<Section[]> = new BehaviorSubject([]);
  get sections$() {
    return this._sections$.pipe(shareReplay());
  }

  constructor(
    private http: HttpClient,
    private cardHttpService: CardHttpService
  ) {
    this.getAllSections();
  }

  getAllSections() {
    this.http.get<Section[]>("/api/v1/sections").subscribe(
      (val) => this._sections$.next(val),
      (error) => {
        //TODO: error handling
      }
    );
  }

  updateSection(id: string, update: UpdateSection) {
    this.http.patch<Section>("/api/v1/sections/" + id, update).subscribe(
      (val) => {
        const sectionsWithoutUpdated = this._sections$.value.filter(
          (s) => s.id != val.id
        );
        this._sections$.next([...sectionsWithoutUpdated, val]);
      },
      (error) => {
        //TODO: error handling
      }
    );
  }

  deleteSection(id: string) {
    this.http.delete<Section>("/api/v1/sections/" + id).subscribe({
      complete: () => {
        this.getAllSections();
        this.cardHttpService.getAllCards();
      },
    });
  }
  createSection(create: CreateSection) {
    this.http.post<Section>("/api/v1/sections", create).subscribe(
      (val) => this._sections$.next([...this._sections$.value, val]),
      (error) => {
        //TODO: error handling
      }
    );
  }
}
