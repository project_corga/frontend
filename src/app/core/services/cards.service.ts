import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Card, CreateCard, UpdateCard } from "../models/card.model";
import { CardHttpService } from "./cards-http.service";
import { SectionService } from "./sections.service";

@Injectable({
  providedIn: "root",
})
export class CardService {
  constructor(
    private httpService: CardHttpService,
    private sectionService: SectionService
  ) {}

  create(card: CreateCard): void {
    this.httpService.createCard(card);
  }

  delete(id: string): void {
    this.httpService.deleteCard(id);
  }

  getFavorites(): Observable<Card[]> {
    return this.httpService.cards$.pipe(
      map((cards) => cards.filter((c) => c.favorite == true))
    );
  }

  update(id: string, data: UpdateCard): void {
    this.httpService.updateCard(id, data);
  }

  setFavorite(id: string, favorite: boolean): void {
    this.httpService.updateCard(id, { favorite });
  }

  getByID(id: string): Observable<Card> {
    return this.httpService.cards$.pipe(
      map((cards) => cards.find((c) => c.id === id))
    );
  }

  getBySectionID(id: string): Observable<Card[]> {
    return this.httpService.cards$.pipe(
      map((cards) => cards.filter((c) => c.sectionId === id))
    );
  }
}
