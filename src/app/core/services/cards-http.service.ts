import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { shareReplay } from "rxjs/operators";
import { Card, CreateCard, UpdateCard } from "../models/card.model";

@Injectable({
  providedIn: "root",
})
export class CardHttpService {
  private _cards$: BehaviorSubject<Card[]> = new BehaviorSubject([]);
  get cards$() {
    return this._cards$.pipe(shareReplay());
  }

  constructor(private http: HttpClient) {
    this.getAllCards();
  }

  getAllCards() {
    this.http.get<Card[]>("/api/v1/cards").subscribe(
      (val) => this._cards$.next(val),
      (error) => {
        //TODO: error handling
      }
    );
  }

  updateCard(id: string, update: UpdateCard) {
    this.http.patch<Card>("/api/v1/cards/" + id, update).subscribe(
      (val) => {
        const cardsWithoutUpdated = this._cards$.value.filter(
          (c) => c.id != val.id
        );
        this._cards$.next([...cardsWithoutUpdated, val]);
      },
      (error) => {
        //TODO: error handling
      }
    );
  }

  deleteCard(id: string) {
    this.http.delete<Card>("/api/v1/cards/" + id).subscribe(
      (val) =>
        this._cards$.next([
          ...this._cards$.value.filter((c) => c.id != val.id),
        ]),
      (error) => {
        //TODO: error handling
      }
    );
  }

  createCard(create: CreateCard) {
    this.http.post<Card>("/api/v1/cards", create).subscribe(
      (val) => this._cards$.next([...this._cards$.value, val]),
      (error) => {
        //TODO: error handling
      }
    );
  }
}
