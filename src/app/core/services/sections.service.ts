import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { CreateSection, Section, UpdateSection } from "../models/section.model";
import { SectionHttpService } from "./sections-http.service";

@Injectable({
  providedIn: "root",
})
export class SectionService {
  constructor(private httpService: SectionHttpService) {}

  create(section: CreateSection): void {
    this.httpService.createSection(section);
  }

  delete(id: string): void {
    this.httpService.deleteSection(id);
  }

  getFavorites(): Observable<Section[]> {
    return this.httpService.sections$.pipe(
      map((sections) => sections.filter((s) => s.favorite == true))
    );
  }

  update(id: string, data: UpdateSection): void {
    this.httpService.updateSection(id, data);
  }

  setFavorite(id: string, favorite: boolean): void {
    this.httpService.updateSection(id, { favorite });
  }

  getRoot(): Observable<Section> {
    return this.httpService.sections$.pipe(
      map((sections) => sections.find((s) => s.parentId == null))
    );
  }

  getTopLevel(): Observable<Section[]> {
    return this.httpService.sections$.pipe(
      map((sections) => {
        const root = sections.find((s) => s.parentId == null);
        return sections.filter((s) => s.parentId === root.id);
      })
    );
  }

  getByID(id: string): Observable<Section> {
    return this.httpService.sections$.pipe(
      map((sections) => sections.find((s) => s.id === id))
    );
  }

  getSubSections(id: string): Observable<Section[]> {
    return this.httpService.sections$.pipe(
      map((sections) => sections.filter((s) => s.parentId === id))
    );
  }

  getPath(id: string): Observable<Section[]> {
    return this.httpService.sections$.pipe(
      map((sections) => {
        const current: Section = sections.find((s) => id === s.id);
        const output: Section[] = [current];
        while (output[output.length - 1].parentId) {
          const section = sections.find(
            (s) => output[output.length - 1].parentId === s.id
          );
          output.push(section);
        }
        output.pop();
        return output;
      })
    );
  }
}
