import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TestBed, waitForAsync } from "@angular/core/testing";
import { of } from "rxjs";
import { mockCard, randomText } from "../../test.util";
import { Card } from "../models/card.model";
import { CardHttpService } from "./cards-http.service";
import { CardService } from "./cards.service";
import { SectionService } from "./sections.service";

describe("CardService", () => {
  let mockCardHttpService: CardHttpService;
  let mockSectionsService: SectionService;
  let cardService: CardService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    mockCardHttpService = TestBed.inject(CardHttpService);
    mockSectionsService = TestBed.inject(SectionService);
    cardService = TestBed.inject(CardService);
  });

  it("should create", () => {
    expect(mockSectionsService).toBeTruthy();
  });

  it("should call http service on setFavorite", async () => {
    let spy = spyOn(mockCardHttpService, "updateCard");

    let id = randomText();
    cardService.setFavorite(id, true);

    expect(spy.calls.count()).toEqual(1);
    expect(spy.calls.mostRecent().args).toEqual([id, { favorite: true }]);
  });

  it("should be correct on getFavorites", async () => {
    let mockData: Card[] = [
      { ...mockCard(), favorite: true },
      mockCard(),
      { ...mockCard(), favorite: true },
      mockCard(),
      mockCard(),
    ];

    spyOnProperty(mockCardHttpService, "cards$").and.returnValue(of(mockData));

    let favorites = await cardService.getFavorites().toPromise();
    expect(favorites).toEqual([mockData[0], mockData[2]]);
  });

  it("should be correct on getCardsByParentID", async () => {
    let mockData: Card[] = [mockCard(), mockCard(), mockCard()];

    spyOnProperty(mockCardHttpService, "cards$").and.returnValue(of(mockData));

    let cards = await cardService
      .getBySectionID(mockData[0].sectionId)
      .toPromise();
    expect(cards).toEqual([mockData[0]]);
  });

  it("should be correct on getById", async () => {
    let mockData: Card[] = [mockCard(), mockCard(), mockCard()];

    spyOnProperty(mockCardHttpService, "cards$").and.returnValue(of(mockData));

    let card = await cardService.getByID(mockData[0].id).toPromise();
    expect(card).toEqual(mockData[0]);
  });

  it("should call http service on update", async () => {
    let spy = spyOn(mockCardHttpService, "updateCard");

    cardService.update(randomText(), {
      favorite: false,
      name: randomText(),
    });

    expect(spy.calls.count()).toEqual(1);
  });

  it("should call http service on delete", async () => {
    let spy = spyOn(mockCardHttpService, "deleteCard");

    cardService.delete(randomText());

    expect(spy.calls.count()).toEqual(1);
  });

  it("should call http service on create", async () => {
    let spy = spyOn(mockCardHttpService, "createCard");

    cardService.create(mockCard());

    expect(spy.calls.count()).toEqual(1);
  });
});
