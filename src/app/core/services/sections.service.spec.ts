import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TestBed, waitForAsync } from "@angular/core/testing";
import { of } from "rxjs";
import { mockSection, randomText } from "../../test.util";
import { Section } from "../models/section.model";
import { SectionHttpService } from "./sections-http.service";
import { SectionService } from "./sections.service";

describe("SectionService", () => {
  let mockSectionHttpService: SectionHttpService;
  let sectionService: SectionService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    mockSectionHttpService = TestBed.inject(SectionHttpService);
    sectionService = TestBed.inject(SectionService);
  });

  it("should create", () => {
    expect(sectionService).toBeTruthy();
  });

  it("should be correct on getFavorites", async () => {
    let mockData: Section[] = [
      { ...mockSection(), favorite: true },
      mockSection(),
      { ...mockSection(), favorite: true },
      mockSection(),
      mockSection(),
    ];

    spyOnProperty(mockSectionHttpService, "sections$").and.returnValue(
      of(mockData)
    );

    let favorites = await sectionService.getFavorites().toPromise();
    expect(favorites).toEqual([mockData[0], mockData[2]]);
  });

  it("should be correct on getRoot", async () => {
    let mockData: Section[] = [
      mockSection(),
      { ...mockSection(), parentId: null },
      mockSection(),
      mockSection(),
    ];

    spyOnProperty(mockSectionHttpService, "sections$").and.returnValue(
      of(mockData)
    );

    let root = await sectionService.getRoot().toPromise();
    expect(root).toEqual(mockData[1]);
  });

  it("should be correct on getTopLevel", async () => {
    let root: Section = { ...mockSection(), parentId: null };
    let mockData: Section[] = [
      root,
      { ...mockSection(), parentId: root.id },
      mockSection(),
      { ...mockSection(), parentId: root.id },
      mockSection(),
      mockSection(),
    ];

    spyOnProperty(mockSectionHttpService, "sections$").and.returnValue(
      of(mockData)
    );

    let topLevel = await sectionService.getTopLevel().toPromise();
    expect(topLevel).toEqual([mockData[1], mockData[3]]);
  });

  it("should be correct on getById", async () => {
    let mockData: Section[] = [mockSection(), mockSection(), mockSection()];

    spyOnProperty(mockSectionHttpService, "sections$").and.returnValue(
      of(mockData)
    );

    let byId = await sectionService.getByID(mockData[0].id).toPromise();
    expect(byId).toEqual(mockData[0]);
  });

  it("should be correct on getSubSections", async () => {
    let mockParent = mockSection();
    let mockData: Section[] = [
      mockParent,
      mockSection(),
      { ...mockSection(), parentId: mockParent.id },
      mockSection(),
    ];

    spyOnProperty(mockSectionHttpService, "sections$").and.returnValue(
      of(mockData)
    );

    let subSections = await sectionService
      .getSubSections(mockParent.id)
      .toPromise();
    expect(subSections).toEqual([mockData[2]]);
  });

  it("should be correct on getPath", async () => {
    let root: Section = { ...mockSection(), parentId: undefined };
    let mockParent: Section = { ...mockSection(), parentId: root.id };
    let mockChild: Section = { ...mockSection(), parentId: mockParent.id };
    let mockData: Section[] = [mockParent, root, mockChild];

    spyOnProperty(mockSectionHttpService, "sections$").and.returnValue(
      of(mockData)
    );

    let subSections = await sectionService.getPath(mockChild.id).toPromise();
    expect(subSections).toEqual([mockChild, mockParent]);
  });

  it("should call http service on create", async () => {
    let spy = spyOn(mockSectionHttpService, "createSection");

    sectionService.create({
      parentId: randomText(),
      favorite: false,
      name: randomText(),
    });

    expect(spy.calls.count()).toEqual(1);
  });

  it("should call http service on update", async () => {
    let spy = spyOn(mockSectionHttpService, "updateSection");

    sectionService.update(randomText(), {
      favorite: false,
      name: randomText(),
    });

    expect(spy.calls.count()).toEqual(1);
  });

  it("should call http service on delete", async () => {
    let spy = spyOn(mockSectionHttpService, "deleteSection");

    sectionService.delete(randomText());

    expect(spy.calls.count()).toEqual(1);
  });

  it("should call http service on setFavorite", async () => {
    let spy = spyOn(mockSectionHttpService, "updateSection");

    let id = randomText();
    sectionService.setFavorite(id, true);

    expect(spy.calls.count()).toEqual(1);
    expect(spy.calls.mostRecent().args).toEqual([id, { favorite: true }]);
  });
});
