export interface Section {
  id: string;
  name: string;
  parentId: string;
  favorite: boolean;
  cardIds: string[];
  subSectionIds: string[];
}

export type UpdateSection = Partial<
  Omit<Section, "id" | "parentId" | "cardIds" | "subSectionIds">
>;

export type CreateSection = Omit<Section, "id" | "cardIds" | "subSectionIds">;
