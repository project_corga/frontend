export interface Card {
  id: string;
  name: string;
  favorite: boolean;
  sectionId: string;
  content: string;
}

export type UpdateCard = Partial<Omit<Card, "id">>;
export type CreateCard = Omit<Card, "id" | "content">;
