import { LayoutModule } from "@angular/cdk/layout";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SharedModule } from "./shared/shared.module";
import { ViewsModule } from "./views/views.module";

@NgModule({
  declarations: [AppComponent],
  imports: [
    SharedModule,
    ViewsModule,
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    HttpClientModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
