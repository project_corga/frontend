import { Card } from "./core/models/card.model";
import { Section } from "./core/models/section.model";

export function mockSection(): Section {
  return {
    id: randomText(),
    cardIds: [],
    favorite: false,
    name: randomText(),
    parentId: randomText(),
    subSectionIds: [],
  };
}

export function randomText(): string {
  return Math.random().toString(36).substring(7);
}

export function mockCard(): Card {
  return {
    id: randomText(),
    favorite: false,
    name: randomText(),
    sectionId: randomText(),
    content: randomText(),
  };
}
