import { ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { AppComponent } from "./app.component";
import { SharedModule } from "./shared/shared.module";

describe("AppComponent", () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let domReference;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, SharedModule],
      declarations: [AppComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    component = fixture.debugElement.componentInstance;
    domReference = fixture.debugElement.nativeElement;
  });

  it("should create the app", () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'CORGA'`, () => {
    expect(component.title).toEqual("CORGA");
  });

  it("should contain footer", () => {
    expect(domReference.querySelector("app-footer")).toBeTruthy();
  });
});
