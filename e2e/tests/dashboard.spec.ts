import {
  createSection,
  expectDeleteSectionDialogAndDelete,
  openDeleteSectionDialog,
  updateSection,
} from "../util/sections";
import { start } from "../util/start";

describe("Dashboard", () => {
  before(() => {
    start();
  });

  it("should show sections in dashboard", () => {
    cy.get("[data-cy=dashboard-main-sections]")
      .should("exist")
      .should("be.visible");
  });

  it("should be possible to create, update and delete a section in dashboard", () => {
    //read
    cy.get("[data-cy=dashboard-main-sections]")
      .within(() => {
        cy.contains("child1").should("exist").should("be.visible");
        cy.contains("child2").should("exist").should("be.visible");
      })
      .should("exist")
      .should("be.visible");
    cy.get("[data-cy=dashboard-favorite-sections]")
      .within(() => {
        cy.contains("child11").should("exist").should("be.visible");
        cy.contains("child2").should("not.exist");
      })
      .should("exist")
      .should("be.visible");
    //create
    const section = createSection("Test", true);
    section.should("exist").should("be.visible");
    //update
    updateSection(section, "updated Section", false)
      .should("exist")
      .should("be.visible");
    //delete
    openDeleteSectionDialog(section);
    expectDeleteSectionDialogAndDelete();
  });

  it("should be possible to mark a section as favorite", () => {
    // setup dummy
    const name = "FavSection";
    createSection(name, false);

    // check dummy is displayed in main sections but not in fav-sections
    cy.get("[data-cy=dashboard-main-sections]").within(() => {
      cy.contains(name).should("exist").should("be.visible");
    });
    cy.get("[data-cy=dashboard-favorite-sections]").within(() => {
      cy.contains(name).should("not.exist");
    });

    // select section and set favorite in header -> navigate back to home
    cy.get("[data-cy=dashboard-main-sections]").contains(name).first().click();
    //section.click({ force: true });
    cy.get("[data-cy=header-favorite-section-button]").click();
    cy.get("[data-cy=header-home-button]").click();

    // check dummy is displayed in main- and fav-sections
    cy.get("[data-cy=dashboard-main-sections]").within(() => {
      cy.contains(name).should("exist").should("be.visible");
    });
    cy.get("[data-cy=dashboard-favorite-sections]").within(() => {
      cy.contains(name).should("exist").should("be.visible");
    });
  });
});
