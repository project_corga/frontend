import "cypress";

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (on, config) => {
  on("before:browser:launch", (browser = {}, launchOptions) => {
    if (browser.name === "chrome") {
      launchOptions.args.push("--lang=en");
      launchOptions.args.push("--disable-site-isolation-trials");
      return launchOptions;
    }
  });
};
