export function interceptSectionRequest(): void {
  cy.intercept("**/api/v1/sections/*").as("SectionRequest");
}

export function waitForConfigurationRequest(): void {
  cy.wait("@SectionRequest");
}
