import { interceptSectionRequest } from "./intercept";

export function start() {
  cy.visit("");
  cy.url().should("eq", "http://localhost:4200/dashboard");
  interceptSectionRequest();
}
