export function createSection(
  name: string,
  favorite: boolean
): Cypress.Chainable {
  openCreateSectionDialog();
  expectCreateSectionDialog();
  cy.get("#mat-input-0")
    .should("be.visible")
    .clear()
    .type(name)
    .blur()
    .should("have.value", name);
  if (favorite) {
    cy.get("#mat-checkbox-1-input").click({ force: true });
  }
  cy.get("[data-cy='create-section-create']")
    .should("be.visible")
    .should("exist")
    .click();
  return cy.contains(name);
}

export function updateSection(
  section: Cypress.Chainable,
  updatedName: string,
  updateFavorite: boolean
): Cypress.Chainable {
  section.get("[data-cy='update-section-button']").first().click();
  /* section.within(() => {
    cy.get("[data-cy='update-section-button']")
      .should("exist")
      .should("be.visible")
      .click();
  }); */
  expectUpdateSectionDialog();
  cy.get("#mat-input-1")
    .should("be.visible")
    .clear()
    .type(updatedName)
    .blur()
    .should("have.value", updatedName);
  if (updateFavorite) {
    cy.get("#mat-checkbox-1-input").click({ force: true });
  }
  cy.get("[data-cy='update-section-update']")
    .should("be.visible")
    .should("exist")
    .click();
  return cy.contains(updatedName);
}

export function openDeleteSectionDialog(section: Cypress.Chainable) {
  section.get("[data-cy='delete-section-button']").first().click();
}

export function openCreateSectionDialog() {
  cy.get("app-create-section-button")
    .should("exist")
    .should("be.visible")
    .click();
}

export function expectCreateSectionDialog() {
  cy.contains("Create Section");
  cy.get("[data-cy='create-section-cancel']")
    .should("be.visible")
    .should("exist");
  cy.get("[data-cy='create-section-create']")
    .should("be.visible")
    .should("exist")
    .should("be.disabled");
}

export function expectUpdateSectionDialog() {
  cy.contains("Update Section");
  cy.get("[data-cy='update-section-cancel']")
    .should("be.visible")
    .should("exist");
  cy.get("[data-cy='update-section-update']")
    .should("be.visible")
    .should("exist")
    .should("be.enabled");
}

export function expectDeleteSectionDialogAndDelete() {
  cy.contains("Delete");
  cy.get("[data-cy='delete-section-cancel']")
    .should("be.visible")
    .should("exist");
  cy.get("[data-cy='delete-section-delete']")
    .should("be.visible")
    .should("exist")
    .should("be.enabled")
    .click();
}
