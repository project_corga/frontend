# Frontend

## Requirements

- npm installed
- a running backend server on port 8080. The source code is available here: https://gitlab.com/project_corga/backend

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
